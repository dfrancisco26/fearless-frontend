function createCard(name, description, pictureUrl, startDate, endDate) {
  return `
    <div class="col-md-4">
      <div class="shadow-lg p-3 mb-5 bg-white rounded">
        <div class="card">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">${startDate} - ${endDate}</div>
        </div>
      </div>
    </div>
  `;
}



window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error("Oh no! Something went wrong.");
    } else {
      const data = await response.json();
      const cardWrapper = document.querySelector('.card-wrapper');

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts).toLocaleDateString();
          const endDate = new Date(details.conference.ends).toLocaleDateString();

          const html = createCard(name, description, pictureUrl, startDate, endDate);
          cardWrapper.innerHTML += html;
        }
      }
    }
  } catch (e) {
    console.error(e.message);
  }
});